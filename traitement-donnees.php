<?php 

/* Améliorations possibles :  */

/*
FRONT : 
 - Prévoir un loader et une ligne d'alerte pour chaque ligne de la table plutôt qu'en-dessous du bouton Enregistrer.

*/

class Traitement_Donnees{
	
	private $champs = array(
		'nom' => array('verif' => 'lettres', 'requis' => 'requis'), 
		'prenom' => array('verif' => 'lettres', 'requis' => 'requis'), 
		'adresse' => array('verif' => '', 'requis' => 'requis'), 
		'code_postal' => array('verif' => 'codep', 'requis' => 'requis'), 
		'ville' => array('verif' => 'lettres', 'requis' => 'requis'), 
		'telephone' => array('verif' => 'tel', 'requis' => 'requis'), 
		'mobile' => array('verif' => 'tel', 'requis' => 'requis')
	);
	
	public $e;
	
	private $error = '';
	
	private $path_folder = 'donnees';
	
	private $file_name = 'data.txt';
	
	private $path_file = '';
	
	private $nom_client = '';
	
	private $prenom_client = '';
	
	private $lignes = array();
	
	
	public function __construct( $data ){
		
		/* Récupération des données */
		if( ! isset($data['action']) ){
			$this->set_error('no_action');
			return;
		}
		
		$action = stripslashes($data['action']);
		
		$this->init();
		
		if( $action == 'nouvelle_ligne' && ! empty($data['data']) ){
			
			$this->create_folder();
			$this->create_file();
			
			$tab_tampon = $this->extraction_donnees($data['data']);
			$chaine = $this->transformation_donnees($tab_tampon);
			$this->ecrire_nouvelle_ligne($chaine);
			
		}elseif( $action == 'supprime_table' ){
			
			$this->supprime_table();
			
		}elseif( $action == 'supprime_ligne' && isset($data['ligne']) ){
			
			$this->supprime_ligne($data['ligne']);
			
		}elseif( $action == 'modif_ligne' && ! empty($data['data']) && isset($data['ligne']) ){
			
			$tab_tampon = $this->extraction_donnees($data['data']);
			$chaine = $this->transformation_donnees($tab_tampon);
			$this->modif_ligne($chaine, $data['ligne']);
		}
	}
	
	
	public function init(){
		
		$this->e = new stdClass();
		
		$this->create_path();
		
		if( file_exists($this->path_file) ){
			
			$new_lignes = $lignes = array();
			$lignes = file($this->path_file);
			$n = 0;
			
			foreach($lignes as $cl => $ligne){
				$ligne = trim($ligne);
				if( ! empty($ligne) ){
					$new_lignes[$n] = $ligne;
					$n++;
				}
			}
				
			$this->lignes = $new_lignes;
		}
	}
	
	
	/* Extraction des données en tableau et vérification */
	
	public function extraction_donnees( $donnees ){
		
		if( ! is_string($donnees) ) return;
		
		$donnees_tab = explode('&', $donnees);
		
		if( empty($donnees_tab) || ! is_array($donnees_tab) ){
			$this->set_error('data_issues');
			return;
		}
		
		$tab_tampon = $this->creation_tableau_tampon();
		
		foreach($donnees_tab as $cl => $vl){
			
			/* Extraction de la clé et la valeur de chaque donnée en tableaux à 2 entrées */
			$donnee = explode('=', $vl);
			if( empty($donnee) || ! is_array($donnee) ){
				$this->set_error('data_issues');
				break;
			}
			
			$key = trim(stripslashes($donnee[0]));
			$value = trim(stripslashes($donnee[1]));
			
			if( array_key_exists($key, $this->champs) ){
				
				$tab_tampon[$key] = $value;
			}
		}
		
		return $tab_tampon;
	}
	
	
	public function transformation_donnees( $tab_tampon ){
		
		if( empty($tab_tampon) || ! is_array($tab_tampon) ) return;
		
		$chaine = '';
		$count = count($tab_tampon);
		$n = 1;
		$t = new stdClass();
		
		foreach($tab_tampon as $key => $value){
			
			$value = $this->verification_donnees( $key, $value, $tab_tampon );
			if( $value === false ){
				$chaine = ''; break;
			}
			
			if( $key == 'nom' ) $this->nom_client = $value;
			if( $key == 'prenom' ) $this->prenom_client = $value;
			
			/* Transformation des données client en chaîne */
			$pipe = '';
			if( $n < $count ) $pipe = ' | ';
			
			$chaine .= $key.' = '.$value.$pipe;
			$n++;
			
			/* Réponse json */
			$t->$key = $value;
		}
		
		$this->e->tab = $t;
		
		return $chaine;
	}
	
	
	public function ecrire_nouvelle_ligne( $chaine ){
		
		if( file_exists($this->path_file) && ! empty($chaine) ){
		    
			if( ! $this->client_deja_enregistre() ){
				
				$this->e->ligne = count($this->lignes);
				
				$ressource = $this->ouverture_fichier('a');
				
				$chaine = $this->insertion_saut_ligne( $chaine );
				$this->ecriture_fichier($ressource, $chaine);
				
				$this->fermeture_fichier($ressource);
			}
		}
	}
	
	
	public function supprime_table(){
		
		if( file_exists($this->path_file) ){
			
			$ressource = $this->ouverture_fichier('w');
			$this->fermeture_fichier($ressource);
			$this->e->okay_supprime_table = 'okay_supprime_table';
		}
	}	
	
	
	public function supprime_ligne( $l ){
		
		if( file_exists($this->path_file) && isset($this->lignes[$l]) ){
			
			$ressource = $this->ouverture_fichier('w');
			
			unset($this->lignes[$l]);
			
			$count = count($this->lignes);
			$n = 0;
				
			if( ! empty($this->lignes) ){
				foreach($this->lignes as $ligne){
					$ligne = $this->insertion_saut_ligne( $ligne );
					$rec = $this->ecriture_fichier($ressource, $ligne);
					if( $rec == 'okay_write' ){
						$n++;
					}
				}
			}
			
			if( $n == $count ){
				$this->e->okay_supprime_ligne = 'okay_supprime_ligne';
			}
			rewind($ressource);
			$this->fermeture_fichier($ressource);
		}
	}
	
	
	public function modif_ligne( $chaine, $l ){
		
		if( file_exists($this->path_file) && isset($this->lignes[$l]) ){
			
			$ressource = $this->ouverture_fichier('w');
			
			$this->lignes[$l] = $chaine;
			
			$count = count($this->lignes);
			$n = 0;
			
			if( ! empty($this->lignes) ){
				foreach($this->lignes as $ligne){
					$ligne = $this->insertion_saut_ligne( $ligne );
					$rec = $this->ecriture_fichier($ressource, $ligne);
					if( $rec == 'okay_write' ){
						$n++;
					}
				}
			}
			
			if( $n == $count ){
				$this->e->okay_modif_ligne = 'okay_modif_ligne';
			}
			
			$this->fermeture_fichier($ressource);
		}
		
	}
	
	
	public function ouverture_fichier( $mode, $error = '' ){
		
		if( empty($error) ) $error = 'file_nopened';
		
		$ressource = fopen($this->path_file, $mode);
		if( $ressource === false ){
			$this->set_error($error, $this->file_name);
		}
		return $ressource;
	}
	
	
	public function ecriture_fichier( $ressource, $ligne ){
		
		if( empty($ligne) ){
			$this->set_error('no_write');
			return;
		}
		
		$nbr_ligne = strlen($ligne);
		
		$nbr_written = fwrite($ressource, $ligne);
		
		if( $nbr_ligne === $nbr_written ){
			$this->e->okay_write = 'okay_write';
			return 'okay_write';
		}elseif( $nbr_written !== false && $nbr_ligne != $nbr_written ){
			$this->set_error('no_write');
		}elseif( $nbr_written === false ){
			$this->set_error('data_issues');
		}
	}
	
	
	public function fermeture_fichier( $ressource ){
		$succes = fclose($ressource);
		if( $succes === false ){
			$this->set_error('file_noclose', $this->file_name);
		}
	}
	
	
	public function creation_tableau_tampon(){
		$tab = array();
		foreach($this->champs as $cl => $vl){
			$tab[$cl] = '';
		}
		return $tab;
	}
	
	
	public function verification_donnees( $key, $value, $tab ){
		
		if( isset($this->champs[$key]) ){
			
			if( $this->champs[$key]['requis'] != 'requis' ) return;
			
			$type = $this->champs[$key]['verif'];
			
			if( $type == 'tel' && empty($tab['telephone']) && empty($tab['mobile']) ){
				$this->set_error('tel_vide'); return false;
			}elseif( empty($value) && $type != 'tel' ){
				$this->set_error('requis', $key); return false;
			}elseif( $type == 'lettres' && preg_match('/[0-9]+/', $value) ){
				$this->set_error($type, $key); return false;
			}elseif( $type == 'codep' && ! preg_match('/^[0-9]{5}$/', $value) ){
				$this->set_error($type, $key); return false;
			}elseif( $type == 'tel' && ! empty($value) && ! preg_match('/^(0[1-9])[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})$/', $value) ){
				$this->set_error($type, $key); return false;
			}elseif( $type == 'tel' && preg_match('/^(0[1-9])[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})$/', $value, $match) ){
				$value = $match[1].' '.$match[2].' '.$match[3].' '.$match[4].' '.$match[5];
			}
			
			return $value;
		}
	}
	
	
	/* Vérifie si le client a déjà été enregistré */
	
	public function client_deja_enregistre(){
		
		if( ! empty($this->lignes) && is_array($this->lignes) ){
			
			foreach($this->lignes as $l => $ligne){
				if( ! empty($ligne) && preg_match('#\b(?<!\-)'.$this->nom_client.'(?!\-)\b#', $ligne) && preg_match('#\b(?<!\-)'.$this->prenom_client.'(?!\-)\b#', $ligne) ){
					$this->set_error('customer_exists');
					$this->e->customer_exists = 'customer_exists';
					$this->e->ligne = $l;
					return true;
				}
			}
		}
		return false;
	}
	
	
	public function insertion_saut_ligne( $chaine ){
		
		/* if( ! $this->fichier_vide() ){
			$nouvelle_ligne = "$chaine\r\n";
		}else{
			$nouvelle_ligne = "$chaine";
		} */
		return "$chaine\r\n";
	}
	
	
	public function fichier_vide(){
		
		if( filesize($this->path_file) == 0 ){
			return true;
		}
		return false;
	}
	
	
	/* Vérification de l'existance du dossier donnees et/ou création du dossier */
	
	public function create_folder(){
		
		if( ! file_exists($this->path_folder) ){
			
			if( ! mkdir($this->path_folder) ){
				$this->set_error('no_folder', $this->path_folder);
			}
		}
	}
	
	
	public function create_file(){
		
		if( ! file_exists($this->path_file) ){
			
			$ressource = $this->ouverture_fichier('w', 'no_file');
			$this->fermeture_fichier($ressource);
		}
	}
	
	
	public function create_path(){
		$this->path_file = $this->path_folder.'/'.$this->file_name;
	}
	
	
	public function json_echo(){
		
		$j = new stdClass();
		if( is_object($this->e) ){
			$j = $this->e;
		}
		
		if( ! empty($this->error) ){
			$j->error = $this->error;
		}
		echo json_encode($j);
	}
	
	
	public function set_error( $key, $value = '' ){
		
		if( empty($key) ) return false;
		
		$message = '';
		
		switch( $key ){
			case 'no_action' : $message = 'Aucune action n\'a été établie.'; break;
			case 'no_write' : $message = 'La ligne n\'a pas été écrite entièrement.'; break;
			case 'customer_exists' : $message = 'Client déjà enregistré.'; break;
			case 'no_folder' : $message = 'Le dossier '.$value.' n\'a pas pu être créé.'; break;
			case 'no_file' : $message = 'Le fichier '.$value.' n\'a pas pu être créé.'; break;
			case 'file_nopened' : $message = 'Le fichier '.$value.' n\'a pas pu être ouvert.'; break;
			case 'lettres' : $message = 'Le champ '.$value.' doit être écrit en toutes lettres.'; break;
			case 'codep' : $message = 'Le champ '.$value.' n\'est pas valide.'; break;
			case 'tel' : $message = 'Le champ '.$value.' doit être un numéro de téléphone valide.'; break;
			case 'tel_vide' : $message = 'Remplir au moins un des deux numéros de téléphone.'; break;
			case 'requis' : $message = 'Le champ '.$value.' est requis.'; break;
			case 'data_issues' : $message = 'Problème de données.'; break;
			case 'file_noclose' : $message = 'Le fichier '.$value.' ne s\'est pas fermé.'; break;
			default : $message = 'Aucune erreur.';
		}
		
		$this->error = $message;
	}
	
	
}

$donnees = new Traitement_Donnees( $_POST );
$donnees->json_echo();
