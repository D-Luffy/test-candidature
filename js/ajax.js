(function($){
	
	var script_url = location.href+'traitement-donnees.php', 
	champs = {
		nom : {
			requis : 'requis', 
			verif : 'lettres'
		}, 
		prenom : {
			requis : 'requis', 
			verif : 'lettres'
		}, 
		adresse : {
			requis : 'requis', 
			verif : ''
		}, 
		code_postal : {
			requis : 'requis', 
			verif : 'codep'
		}, 
		ville : {
			requis : 'requis', 
			verif : 'lettres'
		}, 
		telephone : {
			requis : 'requis', 
			verif : 'tel'
		}, 
		mobile : {
			requis : 'requis', 
			verif : 'tel'
		}
	};
	
	
	$('#submit').on('click', function(e){
		
		e.preventDefault();
		
		var donnees_serialized = amp = '', rec_error = false;
		
		$('.loader').removeClass('load wrong right');
		$('.loader').addClass('load');
		$('#submit').prop('disabled', true);
		$('.message-error').html('');
		$('.tab-rec-ajax tbody tr').removeClass('error');

		/*=== Récupération et vérification des données ===*/
		
		$('.input-form.requis').each(function(id){
			
			if( typeof champs[$(this).attr('name')] != 'undefined' ){
				
				rec_error = erreur_champ( $(this), champs[$(this).attr('name')], $('.form'), 'name' );
				if( rec_error ) return false;
				
				amp = '';
				if( $('.input-form.requis')[id + 1] ){
					amp = '&';
				}
				donnees_serialized += $(this).attr('name')+'='+$(this).val()+amp;
			}
		});
				
		if( rec_error ){
			$('.loader').removeClass('load wrong right');
			$('#submit').prop('disabled', false);
			return false;
		}
		
		
		/*=== Envoi des données en traitement ===*/
		
		$.ajax({
			url : script_url, 
			type  :'POST', 
			dataType : 'json', 
			data : {
				action : 'nouvelle_ligne', 
				data : donnees_serialized
			}, 
			success : function( res ){
				
				if( res ){
					
					$('.loader').removeClass('load wrong right');
					$('#submit').prop('disabled', false);
					
					if( res.error ){
						
						$('.loader').addClass('wrong');
						$('.message-error').append('<p class="error">'+res.error+'<span class="suppr"></span></p>');
						
						if( res.customer_exists && typeof res.ligne != 'undefined' ){
							$('.tab-rec-ajax tbody tr').removeClass('error');
							$('.tab-rec-ajax tbody tr.ligne'+res.ligne).addClass('error');
						}
						
					}else if( res.tab && typeof res.ligne != 'undefined' && res.okay_write ){
						
						$('.loader').addClass('right');
						
						var tr = '';
						
						tr += '<tr data-ligne="'+res.ligne+'" class="ligne'+res.ligne+'">';
						
						for(var val in res.tab){
							tr += '<td><span>'+res.tab[val]+'</span>';
							tr += '<input type="hidden" class="input-modif requis" data-name="'+val+'" data-val="'+res.tab[val]+'" value="'+res.tab[val]+'">';
							tr += '</td>';
						}
						
						tr += '<td class="suppr suppr-ligne" title="Supprimer la ligne"></td>';
						
						tr += '</tr>';
						
						$('.tab-rec-ajax tbody').append(tr);
					}
				}
			}, 
			
			error : function( xhr, status, error ){
				
				$('.loader').removeClass('load wrong right');
				$('#submit').prop('disabled', false);
				$('.loader').addClass('wrong');
				$('.message-error').append('<p class="error">'+status+' | '+error+'<span class="suppr"></span></p>');
			}
			
		}).done(function(){
			
			setTimeout(function(){
				$('.loader').removeClass('load wrong right');
			}, 2600);
			
			$('#submit').prop('disabled', false);
		});
		
	});
	
	
	/*=== Saisie et validation des données ===*/
	
	$(document).on('focus', '.requis', function(){
		
		if( $('.alert-input').length != 0 ){
			
			if( $('.input-form.requis').hasClass('error') ){
				
				$('.input-form.requis').removeClass('error');
				
			}else if(  $('.input-modif.requis').hasClass('error') ){
				
				$('.input-modif.requis').removeClass('error');
			}
			
			$('.alert-input').slideUp(300, function(){
				$(this).remove();
			});
		}
	});
	
	
	function erreur_champ( $this, info, objet, attribut ){
		
		if( info['requis'] != 'requis' ) return;
		
		var verif = info['verif'];
		
		
		
		if( verif == 'tel' && objet.find('input['+attribut+'="telephone"]').val().trim() == '' && $('input['+attribut+'="mobile"]').val().trim() == '' ){
			message($this, 'Remplir au moins un des deux numéros de téléphone');
			return true;
		}else if( $this.val().trim() == '' && verif != 'tel' ){  // On vérifie si les input requis sont remplis
			message($this, 'Remplir le champ vide');
			return true;
		}else if( verif == 'lettres' && /[0-9]+/.test($this.val()) ){
			message($this, 'Remplir ce champ en toutes lettres');
			return true;
		}else if( verif == 'codep' && ! /^[0-9]{5}$/.test($this.val()) ){
			message($this, 'Remplir ce champ avec un code postal valide');
			return true;
		}else if( verif == 'tel' && $this.val().trim() != '' && ! /^(0[1-9])[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})$/.test($this.val()) ){
			message($this, 'Remplir ce champ avec un numéro de téléphone valide');
			return true;
		}else if( verif == 'tel' && /^(0[1-9])[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})[-\.\/\\ ]?([0-9]{2})$/.test($this.val()) ){
			$this.val(RegExp.$1+' '+RegExp.$2+' '+RegExp.$3+' '+RegExp.$4+' '+RegExp.$5);
			if( $this.prev('span').length != 0 ){
				$this.prev('span').html(RegExp.$1+' '+RegExp.$2+' '+RegExp.$3+' '+RegExp.$4+' '+RegExp.$5);
			}
		}
		
		return false;
	}
	
	
	function message($this, message){
		
		if( ! $this.hasClass('error') && $('.alert-input').length == 0 ){
			$this.addClass('error');
			$this.after('<span class="alert-input">'+message+'</span>');
			$('.alert-input').css({'display':'none'});
			$('.alert-input').slideDown(300);
		}
	}
	
	
	$(document).on('click', '.message-error span.suppr', function(){
		$(this).parent('p').remove();
		$('.tab-rec-ajax tbody tr').removeClass('error');
		$('.loader').removeClass('load wrong right');
	});
	
	
	/*=== Comportement table des données ===*/
	
	$('.suppr-table').on('click', function(e){
		
		e.preventDefault();
		
		var tbody, tr = '';
		
		tbody = $(this).parents('table').find('tbody');
		tr = tbody.find('tr');
		
		if( tr.length != 0 ){
			
			if( confirm('Vous allez supprimer toutes la table de données.') ){
				
				$('.message-error').html('');
				
				$.ajax({
					url : script_url, 
					type  :'POST', 
					dataType : 'json', 
					data : {
						action : 'supprime_table'
					}, 
					success : function( res ){
						
						if( res ){
							
							if( res.error ){
								
								$('.message-error').append('<p class="error">'+res.error+'<span class="suppr"></span></p>');
								
							}else if( res.okay_supprime_table ){
								
								tbody.html('');
								$('.message-error').append('<p class="saved">Table supprimée avec succès.<span class="suppr"></span></p>');
							}
						}
					}, 
					
					error : function( xhr, status, error ){
						
						$('.message-error').append('<p class="error">'+status+' | '+error+'<span class="suppr"></span></p>');
					}
					
				});
			}
		}
		
	});
	
	
	$(document).on('click', '.suppr-ligne', function(e){
		
		e.preventDefault();
		
		var tr, ligne = '';
		
		tr = $(this).parent('tr');
		ligne = tr.data('ligne');
		
		$('.message-error').html('');
		
		$.ajax({
			url : script_url, 
			type  :'POST', 
			dataType : 'json', 
			data : {
				action : 'supprime_ligne', 
				ligne : ligne
			}, 
			success : function( res ){
				
				if( res ){
					
					if( res.error ){
						
						$('.message-error').append('<p class="error">'+res.error+'<span class="suppr"></span></p>');
						
					}else if( res.okay_supprime_ligne ){
						
						tr.remove();
						$('.message-error').append('<p class="saved">Ligne supprimée avec succès.<span class="suppr"></span></p>');
						
						$('.tab-rec-ajax tbody tr').each(function(id){
							$(this).removeClass();
							$(this).addClass('ligne'+id);
							$(this).data('ligne', id);
							$(this).attr('data-ligne', id);
						});
					}
				}
			}, 
			
			error : function( xhr, status, error ){
				
				$('.message-error').append('<p class="error">'+status+' | '+error+'<span class="suppr"></span></p>');
			}
			
		});
	});
	
	
	$(document).on('click', '.tab-rec-ajax tbody td', function(e){
		
		e.preventDefault();
		
		if( $('.tab-rec-ajax input[type="text"]').length == 0 ){
			
			$(this).find('span').html('');
			
			$(this).find('.input-modif').attr('type', 'text');
			
			$('.input-modif').focus();
		}
	});
	
	
	$(document).on('blur', '.tab-rec-ajax input[type="text"]', function(e){
		
		e.preventDefault();
		
		var input, value, old_value, tr, td, ligne, key, donnees_serialized = amp = '', rec_error = false;
		
		input = $(this);
		key = input.data('name');
		value = input.val();
		old_value = input.data('val');
		tr = input.parents('tr');
		td = input.parent('td');
		ligne = tr.data('ligne');
		
		$('.message-error').html('');
		
		tr.find('.input-modif.requis').each(function(id){
			
			if( typeof champs[$(this).data('name')] != 'undefined' ){
				
				rec_error = erreur_champ( $(this), champs[$(this).data('name')], tr, 'data-name' );
				if( rec_error ) return false;
				
				amp = '';
				if( tr.find('.input-modif.requis')[id + 1] ){
					amp = '&';
				}
				donnees_serialized += $(this).data('name')+'='+$(this).val()+amp;
				
				if( $(this).data('name') == key ){
					value = $(this).val();
				}
			}
		});
		
		
		if( value != old_value ){
						
			if( rec_error ){
				return false;
			}
			
			$.ajax({
				url : script_url, 
				type : 'POST', 
				dataType : 'json', 
				data : {
					action : 'modif_ligne', 
					data : donnees_serialized, 
					ligne : ligne
				}, 
				success : function( res ){
					
					if( res ){
						
						if( res.error ){
							
							$('.message-error').append('<p class="error">'+res.error+'<span class="suppr"></span></p>');
							
						}else if( res.okay_modif_ligne ){
							
							td.find('span').html(value);
							td.find('.input-modif.requis').attr('type', 'hidden').val(value).data('val', value);
							$('.message-error').append('<p class="saved">Ligne modifiée avec succès.<span class="suppr"></span></p>');
						}
					}
				}, 
				
				error : function( xhr, status, error ){
					
					$('.message-error').append('<p class="error">'+status+' | '+error+'<span class="suppr"></span></p>');
				}
				
			});
			
		}else{
			
			td.find('span').html(value);
			tr.find('.input-modif.requis').attr('type', 'hidden');
		}
	});
	
	
	
	$(window).on('load', function(){
		$('.input-modif').each(function(){
			var val;
			
			val = $(this).prev('span').html().trim();
			
			$(this).val(val);
		});
	});	
	
	
})(jQuery)