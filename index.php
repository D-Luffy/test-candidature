<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Test candidature</title>
<link type="text/css" media="all" rel="stylesheet" href="style.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>

<h1>Test Candidature</h1>

<h2>Formulaire de saisie</h2>

<form method="post" action="" class="form">
	
    <div class="flex-row">
      <div>
        <input type="text" class="input-form requis" name="nom" placeholder="Nom" maxlength="255">
      </div>
      <div>
        <input type="text" class="input-form requis" name="prenom" placeholder="Prénom" maxlength="255">
      </div>
    </div>
    
    <div class="flex-row ww">
      <div>
        <input type="text" class="input-form requis" name="adresse" placeholder="Adresse Complète" maxlength="255">
      </div>
    </div>
    
    <div class="flex-row">
      <div>
        <input type="text" class="input-form requis" name="code_postal" placeholder="Code Postal" maxlength="5">
      </div>
      <div>
        <input type="text" class="input-form requis" name="ville" placeholder="Ville" maxlength="255">
      </div>
    </div>
    
    <div class="flex-row">
      <div>
        <input type="text" class="input-form requis" name="telephone" placeholder="Téléphone" maxlength="14">
      </div>
      <div>
        <input type="text" class="input-form requis" name="mobile" placeholder="Mobile" maxlength="14">
      </div>
    </div>
    
    <div class="flex-row submit">
      <div>
        <input id="submit" type="submit" value="Enregistrer">
      </div>
      <div class="loader">
      </div>
    </div>

</form>

<?php 
function tab( $tab = null, $decal = false ){
	
	$margin_left = '';
	if( is_int($decal) ){
		$margin_left = 'margin-left:'.$decal.'px;';
	}
	
	if( is_array($tab) || is_object($tab) ){
		echo '<div style="background:rgba(255,255,255,0.6);min-width:400px;'.$margin_left.'"><pre>';
		echo print_r($tab);
		echo '</pre></div>';
	}else if( is_string($tab) || is_int($tab) ){
		echo '<p style="background:rgba(255,255,255,0.6);'.$margin_left.'">'.$tab.'</p>';
	}elseif( is_bool($tab) ){
		if( $tab === true ){
			$bool = 'true';
		}elseif( $tab === false ){
			$bool = 'false';
		}
		echo '<p style="background:rgba(255,255,255,0.6);'.$margin_left.'">'.$bool.'</p>';
	}else{
		echo '<p style="background:rgba(255,255,255,0.6);'.$margin_left.'">R.A.S</p>';
	}
	
}
?>

<div class="message-error">
</div>


<h2>Table des données clients</h2>

<div>
<table class="tab-rec-ajax">
  <thead>
    <tr>
      <td>Nom</td>
      <td>Prénom</td>
      <td>Adresse</td>
      <td>Cp</td>
      <td>Ville</td>
      <td>Téléphone</td>
      <td>Mobile</td>
      <td class="suppr suppr-table" title="Supprimer toute la table"></td>
    </tr>
  </thead>
  
  <tbody>
  <?php $path_file = 'donnees/data.txt';?>
  
  <?php if( $lignes = @file($path_file) ){?>
  
  <?php foreach($lignes as $l => $ligne){?>
      
      <?php $ligne = trim($ligne);?>
      
      <?php if( ! empty($ligne) ){?>
          
          <tr data-ligne="<?php echo $l;?>" class="ligne<?php echo $l;?>">
          
          <?php $ligne_tab = explode(' | ', $ligne);?>
          
          <?php if( ! empty($ligne_tab) && is_array($ligne_tab) ){?>
          
          <?php foreach($ligne_tab as $val){?>
          
              <?php $val = trim($val);?>
              
              <?php if( ! empty($val) ){?>
                
				<?php $vl = explode(' = ', $val);?>
                
                <?php if( ! empty($vl[0]) ){?>
                
                  <?php $data_name = $value = '';?>
                  <?php $data_name = trim(str_replace('=', '', $vl[0]));?>
                  <?php if( ! empty($vl[1]) ){ $value = trim($vl[1]); }?>
                  <td>
                    <span><?php echo $value;?></span>
                    <input type="hidden" class="input-modif requis" data-name="<?php echo $data_name;?>" data-val="<?php echo $value;?>" value="<?php echo $value;?>">
                  </td>
                  
                <?php }?>
                
              <?php }?>
              
          <?php }?>
          
          <?php }?>
          
          <td class="suppr suppr-ligne" title="Supprimer la ligne"></td>
          
          </tr>
          
      <?php }?>
    
  <?php }?>
  
  <?php }?>
  </tbody>
</table>
</div>

<script type="text/javascript" src="js/ajax.js"></script>

</body>
</html>